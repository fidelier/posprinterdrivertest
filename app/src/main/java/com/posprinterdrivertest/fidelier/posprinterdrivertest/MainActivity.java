package com.posprinterdrivertest.fidelier.posprinterdrivertest;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {
    private static String internalClassName = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final String dataToPrint = "$big$This is a printer test$intro$posprinterdriver.com$intro$a á à ä e é è ë i í ì ï o ó ò ö u ú ù ü $intro$$cut$$intro$";

        final String BtDevice = "10:00:E8:6B:E2:5A";

        final String printerIpAddress = "192.168.9.150";
        final String printerPort = "9100";

        final String usbDeviceID = "1002";
        final String usbProductID = "20497";
        final String usbVendorId = "1046";

        EditText btDeviceAddres = null;

        Button buttonPrintToDefaultPrinter = (Button) findViewById(R.id.buttonPrintToDefaultPrinter);
        Button buttonPrintToBTPrinter = (Button) findViewById(R.id.buttonPrintToBTPrinter);
        Button buttonPrinterToIpPrinter = (Button) findViewById(R.id.buttonPrinterToIpPrinter);
        Button buttonPrintToUSBPrinter = (Button) findViewById(R.id.buttonPrintToUSBPrinter);

        Button buttonPrintToBTPrinterLink = (Button) findViewById(R.id.buttonPrintToBTPrinterLINK);
        Button buttonPrinterToIpPrinterLink = (Button) findViewById(R.id.buttonPrinterToIpPrinterLink);
        //Button buttonPrintToUSBPrinter = (Button) findViewById(R.id.buttonPrintToUSBPrinter);


        // send data in dataToPrint to default printer in PosPrinterDriver
        buttonPrintToDefaultPrinter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i(internalClassName, "buttonPrintToDefaultPrinter Pressed");
                sendDataToDefaultPrinter(dataToPrint);
            }
        });
        // send data in dataToPrint to BT printer with id= BtDevice xx:xx:xx:xx:xx:xx
        buttonPrintToBTPrinter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i(internalClassName, "buttonPrintToBTPrinter Pressed");
                sendDataToBTPrinter(dataToPrint, BtDevice);
            }
        });
        // send data in dataToPrint to BT printer with id= BtDevice xx:xx:xx:xx:xx:xx
        buttonPrintToBTPrinterLink.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i(internalClassName, "buttonPrintToBTPrinterLink Pressed");
                String dataToPrintParams = "";
                dataToPrintParams = "$printer_type_id=4$";
                dataToPrintParams = dataToPrintParams + "$printer_bt_adress=" + BtDevice + "$" + dataToPrint;
                sendDataToDefaultPrinter(dataToPrintParams);
            }
        });
        // send data in dataToPrint to BT printer with id= BtDevice xx:xx:xx:xx:xx:xx
        buttonPrinterToIpPrinterLink.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i(internalClassName, "buttonPrinterToIpPrinterLink Pressed");
                String dataToPrintParams = "";
                dataToPrintParams = "$printer_type_id=1$";
                dataToPrintParams = dataToPrintParams + "$printer_ip=" + printerIpAddress + "$" ;
                dataToPrintParams = dataToPrintParams + "$printer_port=" + printerPort + "$" + dataToPrint;
                sendDataToDefaultPrinter(dataToPrintParams);
            }
        });
        buttonPrinterToIpPrinter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i(internalClassName, "buttonPrinterToIpPrinter Pressed");
                sendDataToIPPrinter(dataToPrint, printerIpAddress, printerPort);
            }
        });


        // send data in dataToPrint to BT printer with id= BtDevice xx:xx:xx:xx:xx:xx
        buttonPrintToUSBPrinter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i(internalClassName, "buttonPrintToUSBPrinter Pressed");
                sendDataToUsbPrinter(dataToPrint, usbProductID, usbVendorId, usbDeviceID);
            }
        });

    }

    public void sendDataToDefaultPrinter(String textoToSend) {
        Intent intentPrint = new Intent();
        intentPrint.setAction(Intent.ACTION_SEND);
        intentPrint.putExtra(Intent.EXTRA_TEXT, textoToSend);
        intentPrint.setType("text/plain");
        Log.i(internalClassName, "buttonPrintToDefaultPrinter Start Intent");

        this.startActivity(intentPrint);

    }

    public void sendDataToIPPrinter(String textoToSend, String printerIpAddress, String printerPort) {
        String dataToPrint = "$big$This is a printer test$intro$posprinterdriver.com$intro$$intro$$cut$$intro$";
        Intent intentPrint = new Intent();
        intentPrint.setAction(Intent.ACTION_SEND);
        intentPrint.putExtra(Intent.EXTRA_TEXT, textoToSend);
        intentPrint.putExtra("printer_type_id", "1");// For IP
        intentPrint.putExtra("printer_ip", printerIpAddress);
        intentPrint.putExtra("printer_port", printerPort);

        intentPrint.setType("text/plain");
        Log.i(internalClassName, "sendDataToIPPrinter Start Intent");
        this.startActivity(intentPrint);

    }

    public void sendDataToSerialPrinter(String textoToSend) {
        String dataToPrint = "$big$This is a printer test$intro$posprinterdriver.com$intro$$intro$$cut$$intro$";
        Intent intentPrint = new Intent();
        intentPrint.setAction(Intent.ACTION_SEND);
        intentPrint.putExtra(Intent.EXTRA_TEXT, textoToSend);
        intentPrint.putExtra("printer_type_id", "2");// For Serial

    }

    public void sendDataToUsbPrinter(String textoToSend, String usbProductID, String usbVendorId, String usbDeviceID) {
        String dataToPrint = "$big$This is a printer test$intro$posprinterdriver.com$intro$$intro$$cut$$intro$";
        Intent intentPrint = new Intent();
        intentPrint.setAction(Intent.ACTION_SEND);
        intentPrint.putExtra(Intent.EXTRA_TEXT, textoToSend);
        intentPrint.putExtra("printer_type_id", "3");// For USB
        intentPrint.putExtra("printer_usb_product_id", usbProductID);
        intentPrint.putExtra("printer_usb_vendor_id", usbVendorId);
        intentPrint.putExtra("printer_usb_device_id", usbDeviceID);

        intentPrint.setType("text/plain");
        Log.i(internalClassName, "buttonPrintToDefaultPrinter Start Intent");
        this.startActivity(intentPrint);

    }

    public void sendDataToBTPrinter(String textoToSend, String BtDevice) {
        String dataToPrint = "$big$This is a printer test$intro$posprinterdriver.com$intro$$intro$$cut$$intro$";
        Intent intentPrint = new Intent();
        intentPrint.setAction(Intent.ACTION_SEND);
        intentPrint.putExtra(Intent.EXTRA_TEXT, textoToSend);
        intentPrint.putExtra("printer_type_id", "4");// For bluetooth
        intentPrint.putExtra("printer_bt_adress", BtDevice);
        intentPrint.setType("text/plain");
        Log.i(internalClassName, "sendDataToBTPrinter Start Intent");
        this.startActivity(intentPrint);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
